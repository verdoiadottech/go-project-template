# Welcome to go-project-template

[![License][license-badge]][license]
[![GitLab pipeline status][gitlab-pipeline-status]][gitlab-pipeline-report]
[![Gitlab coverage report][gitlab-coverage-report]][gitlab-pipeline-report]
[![Quality Gate Status][sonar-quality-gate]][sonar-project]
[![Sonar Coverage][sonar-coverage]][sonar-project]
[![CodeClimate Maintainability][cc-maintainability-badge]][cc-maintainability]
[![CodeClimate Test Coverage][cc-coverage-badge]][cc-coverage]

[**go-project-template**][project-home] is a template project for my personal [go][go]
projects based on the [Standard Go Project Layout][go-standards] philosophy.

This solves my very first interrogation when creating a new go project :

> From which recent project can I copy the CI pipeline including linters.... ?!

And the answer is now : **HERE !** since this project should contain the most up-to-date
CI pipeline.

## Philosophy

- This project must be easy to understand and hack.
- Since the choice of IDE can be a religious question, this project is IDE agnostic.
- Because some times we cannot choose the CI engine (CTO guidelines, free one, etc...),
  the pipeline must be independent of the used CI engine
- The pipeline must be locally executable and depends only on a posix shell, **git** and
  **docker**.

## About the implemented software

**g**o-**p**roject-**t**emplate produce only one binary: **gpt**.

gpt goal is to expose a simple calculator with [elementary arithmetic][arithmetic] and a
single memory slot through web services.

Used technologies :

- Service exposition technologies :
  - [ ] [GraphQL](https://graphql.org/)
  - [ ] [gRPC](https://grpc.io/)
  - [X] [REST](https://en.wikipedia.org/wiki/Representational_state_transfer)-ish
  - [ ] [SOAP](https://en.wikipedia.org/wiki/SOAP)
- Auth system :
  - [ ] [JWT](https://jwt.io/)
  - [ ] [ldap](https://en.wikipedia.org/wiki/Lightweight_Directory_Access_Protocol)
  - [ ] [OAuth2](https://en.wikipedia.org/wiki/OAuth)
  - [ ] [OpenID](https://en.wikipedia.org/wiki/OpenID)

[arithmetic]: https://en.wikipedia.org/wiki/Elementary_arithmetic
[cc-coverage-badge]: https://api.codeclimate.com/v1/badges/63cff5edf639fa62a92f/test_coverage
[cc-coverage]: https://codeclimate.com/github/verdoiadottech/go-project-template/test_coverage
[cc-maintainability-badge]: https://api.codeclimate.com/v1/badges/63cff5edf639fa62a92f/maintainability
[cc-maintainability]: https://codeclimate.com/github/verdoiadottech/go-project-template/maintainability
[gitlab-coverage-report]: https://gitlab.com/verdoiadottech/go-project-template/badges/master/coverage.svg
[gitlab-pipeline-report]: https://gitlab.com/verdoiadottech/go-project-template/-/pipelines?scope=branches&ref=master
[gitlab-pipeline-status]: https://gitlab.com/verdoiadottech/go-project-template/badges/master/pipeline.svg
[go-standards]: https://github.com/golang-standards/project-layout
[go]: https://golang.org
[license-badge]: https://img.shields.io/badge/license-GPL--3.0-brightgreen
[license]: https://gitlab.com/verdoiadottech/go-project-template/blob/master/COPYING.md
[project-home]: https://gitlab.com/verdoiadottech/go-project-template
[sonar-coverage]: https://sonarcloud.io/api/project_badges/measure?project=verdoiadottech_go-project-template&metric=coverage
[sonar-project]: https://sonarcloud.io/dashboard?id=verdoiadottech_go-project-template
[sonar-quality-gate]: https://sonarcloud.io/api/project_badges/measure?project=verdoiadottech_go-project-template&metric=alert_status
