#!/usr/bin/env sh
# Copyright © 2020 VERDOÏA Laurent
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -o errexit
set -o nounset

if command -v git > /dev/null; then
  PI_COMMIT_REF_NAME=$(git rev-parse --abbrev-ref HEAD)
  export PI_COMMIT_REF_NAME

  PI_COMMIT_SHA=$(git rev-parse HEAD)
  export PI_COMMIT_SHA
else
  # git hash-object -t tree /dev/null
  export PI_COMMIT_SHA=4b825dc642cb6eb9a060e54bf8d69288fbee4904
fi
