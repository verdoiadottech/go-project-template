#!/usr/bin/env sh
# Copyright © 2020 VERDOÏA Laurent
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -o errexit
set -o nounset

# Be sure to be at the root folder of the repository.
cd "$(realpath "$(dirname "$0")/../../..")"

. ./build/ci/scripts/init.sh

if [ -z "${SONAR_TOKEN:-}" ]; then
  echo "SONAR_TOKEN not found. Skip analysis."
  exit 2
fi

export SONAR_USER_HOME="${PWD}/.cache/sonar/home"
if [ ! -d "${SONAR_USER_HOME}" ]; then
  mkdir -p "${SONAR_USER_HOME}"
fi

sonar_working_directory="${PWD}/.cache/sonar/workdir"
if [ ! -d "${sonar_working_directory}" ]; then
  mkdir -p "${sonar_working_directory}"
fi

if [ "${PI_PLATFORM}" = "local" ]; then
  branch_name=local
else
  branch_name=${PI_COMMIT_REF_NAME}
fi

projectVersion=$(git describe --tags HEAD)

sonar-scanner \
  -Dproject.settings=build/ci/jobs/sonar-scanner.properties \
  -Dsonar.branch.name="${branch_name}" \
  -Dsonar.projectVersion="${projectVersion}" \
  -Dsonar.working.directory="${sonar_working_directory}" \
  "$@"
