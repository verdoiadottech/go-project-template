#!/usr/bin/env sh
# Copyright © 2020 VERDOÏA Laurent
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# This script is not intended to be run on the local machine.
# This permit to ensure that go dependencies modules are downloaded only one time.

set -o errexit
set -o nounset

# Be sure to be at the root folder of the repository.
cd "$(realpath "$(dirname "$0")/../../..")"

. ./build/ci/scripts/init.sh

# Populate the cache
go mod download -x

# Verify the cache
go mod verify
