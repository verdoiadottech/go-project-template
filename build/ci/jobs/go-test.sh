#!/usr/bin/env bash
# Copyright © 2020 VERDOÏA Laurent
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -o errexit
set -o nounset

# Be sure to be at the root folder of the repository.
cd "$(realpath "$(dirname "$0")/../../..")"

. ./build/ci/scripts/init.sh

OUTPUT_DIR=dist/go-test
OUT_FILE=${OUTPUT_DIR}/coverage.out
HTML_FILE=${OUTPUT_DIR}/coverage.html

mkdir -p "${OUTPUT_DIR}"

package=$(head -n1 go.mod | cut -d ' ' -f 2)

if go test -coverpkg="${package}/..." -coverprofile="${OUT_FILE}" ./...; then
  testStatus=0
else
  testStatus=1
fi

go tool cover -func="${OUT_FILE}"
go tool cover -html="${OUT_FILE}" -o "${HTML_FILE}"

exit ${testStatus}
