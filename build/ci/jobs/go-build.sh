#!/usr/bin/env sh
# Copyright © 2020 VERDOÏA Laurent
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -o errexit
set -o nounset

# Be sure to be at the root folder of the repository.
cd "$(realpath "$(dirname "$0")/../../..")"

. ./build/ci/scripts/init.sh

_build() {
  export GOOS=$1
  export GOARCH=$2
  shift 2

  echo "Build for: ${GOOS}/${GOARCH}"

  go build \
    -ldflags="${ldflags}" \
    "$@"
}

rm -rf dist/go-build
mkdir -p dist/go-build

package=$(head -n1 go.mod | cut -d ' ' -f 2)/cmd

ldflags="-s -w"
ldflags="${ldflags} -X '${package}.commit=${PI_COMMIT_SHA}'"

if [ -n "${PI_COMMIT_TAG:-}" ]; then
  ldflags="${ldflags} -X '${package}.version=${PI_COMMIT_TAG}'"
fi

ldflags="${ldflags} -X '${package}.date=$(date -u "+%FT%TZ")'"
ldflags="${ldflags} -X '${package}.builtBy=${PI_PLATFORM}'"

_build linux amd64 -o dist/go-build/go-valkey-admin-_linux_amd64
_build darwin amd64 -o dist/go-build/go-valkey-admin_darwin_amd64
