#!/usr/bin/env bash
# Copyright © 2020 VERDOÏA Laurent
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -o errexit
set -o nounset

# Be sure to be at the root folder of the repository.
cd "$(realpath "$(dirname "$0")/../../..")"

. ./build/ci/scripts/init.sh

package=$(head -n1 go.mod | cut -d ' ' -f 2)

mkdir -p dist

CC_VERSION=0.7.0
CC_SHA256=c40e84074037d4409e4c36360f41fa387a00fe0818f110a283ffd018c1da6d9b
CC_TEST_REPORTER=.cache/cc-test-reporter

mkdir -p .cache
if [ ! -f "${CC_TEST_REPORTER}" ]; then
  wget \
    -O "${CC_TEST_REPORTER}" \
    "https://codeclimate.com/downloads/test-reporter/test-reporter-${CC_VERSION}-linux-amd64"
fi

chmod +x "${CC_TEST_REPORTER}"

if [ "$(sha256sum < ${CC_TEST_REPORTER})" != "${CC_SHA256}  -" ]; then
  echo "Cannot verify checksum of ${CC_TEST_REPORTER}"
  exit 1
fi

"${CC_TEST_REPORTER}" before-build

"${CC_TEST_REPORTER}" format-coverage \
  --input-type gocov \
  --prefix "${package}" \
  --output dist/code-climate.coverage.json \
  dist/go-test/coverage.out

if [ "${PI_PLATFORM}" = "local" ]; then
  echo "Do not upload localy generated report"
  exit
fi

if [ "${PI_COMMIT_REF_NAME}" != "master" ]; then
  echo "Upload report only for branch: master"
  exit
fi

if [ -z "${CC_TEST_REPORTER_ID:-}" ]; then
  echo "CC_TEST_REPORTER_ID is not defined"
  exit 2
fi

"${CC_TEST_REPORTER}" upload-coverage \
  --input dist/code-climate.coverage.json
