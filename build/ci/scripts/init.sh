#!/usr/bin/env sh
# Copyright © 2020 VERDOÏA Laurent
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# We expect to be run at the root of the repository
# Just try to check current working directory...
if [ ! -f build/ci/scripts/init.sh ]; then
  echo "Cannot found 'init.sh' from current working directory: ${PWD}"
  exit 1
fi

# A CI platform is detection is delegated to scripts which match to:
# build/ci/platforms/<platform-name>/detect.sh
# 0 should be returned when platform is detected.
# Any other value stand for 'not detected'.

PI_PLATFORM=
for file in build/ci/platforms/*/detect.sh; do
  if [ ! -x "${file}" ]; then
    # This can happen happen when globbing do not match any files.
    continue
  fi

  if "${file}"; then
    PI_PLATFORM=$(basename "$(dirname "${file}")")
    break
  fi
done

if [ -z "${PI_PLATFORM}" ]; then
  echo "Could not detect CI platform" >&2
  exit 1
fi

# Let the env.sh of concerned platform to map specific environment variables into platform
# agnostic PI_XXX one.
# shellcheck source=/dev/null
. "build/ci/platforms/${PI_PLATFORM}/env.sh"

case "${PI_DEBUG_TRACE:-}" in
  [tT][rR][uU][eE] | yes) PI_DEBUG_TRACE=true ;;
  *) PI_DEBUG_TRACE=false ;;
esac

if [ "${PI_DEBUG_TRACE}" = true ]; then
  set -o xtrace
  env
fi

echo "Detected CI platform: ${PI_PLATFORM}"
