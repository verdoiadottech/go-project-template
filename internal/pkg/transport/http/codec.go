// Copyright © 2020 VERDOÏA Laurent
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package http

import (
	"context"
	"encoding/json"
	"errors"
	"net/http"

	"gitlab.com/verdoiadottech/go-project-template/internal/pkg/service"
)

func decodeAdditionRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var req service.AdditionRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func decodeAdditionResponse(_ context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode != http.StatusOK {
		return nil, errors.New(r.Status)
	}
	var req service.AdditionResponse
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func decodeSubtractionRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var req service.SubtractionRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func decodeSubtractionResponse(_ context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode != http.StatusOK {
		return nil, errors.New(r.Status)
	}
	var req service.SubtractionResponse
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func decodeMultiplicationRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var req service.MultiplicationRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func decodeMultiplicationResponse(_ context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode != http.StatusOK {
		return nil, errors.New(r.Status)
	}
	var req service.MultiplicationResponse
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func decodeDivisionRequest(_ context.Context, r *http.Request) (interface{}, error) {
	var req service.DivisionRequest
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}

func decodeDivisionResponse(_ context.Context, r *http.Response) (interface{}, error) {
	if r.StatusCode != http.StatusOK {
		return nil, errors.New(r.Status)
	}
	var req service.DivisionResponse
	err := json.NewDecoder(r.Body).Decode(&req)
	return req, err
}
