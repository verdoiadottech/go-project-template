// Copyright © 2020 VERDOÏA Laurent
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package http

import (
	"net/url"

	"github.com/go-kit/kit/endpoint"
	"github.com/go-kit/kit/transport/http"
)

// ClientSet collects all of the endpoints that compose an add service.
type ClientSet struct {
	AdditionEndpoint       endpoint.Endpoint
	SubtractionEndpoint    endpoint.Endpoint
	MultiplicationEndpoint endpoint.Endpoint
	DivisionEndpoint       endpoint.Endpoint
}

// NewClientSet create a new set of HTTP handler that compose a CalculatorService.
func NewClientSet(address string) ClientSet {
	u := &url.URL{}
	u.Scheme = "http"
	u.Host = address
	u.Path = "/api/"

	set := ClientSet{}

	set.AdditionEndpoint = makeAdditionEndpoint(urlPath(u, "addition"))
	set.SubtractionEndpoint = makeSubtractionEndpoint(urlPath(u, "subtraction"))
	set.MultiplicationEndpoint = makeMultiplicationEndpoint(urlPath(u, "multiplication"))
	set.DivisionEndpoint = makeDivisionEndpoint(urlPath(u, "division"))

	return set
}

func makeAdditionEndpoint(u *url.URL) endpoint.Endpoint {
	return http.NewClient(
		"POST",
		u,
		http.EncodeJSONRequest,
		decodeAdditionResponse,
	).Endpoint()

}

func makeSubtractionEndpoint(u *url.URL) endpoint.Endpoint {
	return http.NewClient(
		"POST",
		u,
		http.EncodeJSONRequest,
		decodeSubtractionResponse,
	).Endpoint()
}

func makeMultiplicationEndpoint(u *url.URL) endpoint.Endpoint {
	return http.NewClient(
		"POST",
		u,
		http.EncodeJSONRequest,
		decodeMultiplicationResponse,
	).Endpoint()
}

func makeDivisionEndpoint(u *url.URL) endpoint.Endpoint {
	return http.NewClient(
		"POST",
		urlPath(u, "division"),
		http.EncodeJSONRequest,
		decodeDivisionResponse,
	).Endpoint()
}

func urlPath(base *url.URL, path string) *url.URL {
	u := &url.URL{Path: path}

	return base.ResolveReference(u)
}
