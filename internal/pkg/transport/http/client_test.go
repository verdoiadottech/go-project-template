// Copyright © 2020 VERDOÏA Laurent
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package http

import (
	"context"
	stdhttp "net/http"
	"net/http/httptest"
	"net/url"
	"reflect"
	"testing"

	"gitlab.com/verdoiadottech/go-project-template/internal/pkg/service"
)

func Test_makeAdditionEndpoint(t *testing.T) {
	goodServer := newStringServer(`{"Result": 42}`)
	defer goodServer.Close()
	badServer := newStringServer("")
	defer badServer.Close()

	tests := []struct {
		name     string
		server   *httptest.Server
		request  interface{}
		response service.AdditionResponse
		wantErr  bool
	}{
		{
			"normal",
			goodServer,
			service.AdditionRequest{
				A: 40,
				B: 2,
			},
			service.AdditionResponse{
				Result: 42,
			},
			false,
		},
		{
			name:    "badServer",
			server:  badServer,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u, err := url.Parse(tt.server.URL)
			if err != nil {
				t.Fatal("Cannot parse httptest server url:", tt.server.URL)
			}

			endpoint := makeAdditionEndpoint(u)
			payload, err := endpoint(context.Background(), tt.request)
			if (err != nil) != tt.wantErr {
				t.Fatalf("endpoint call error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.wantErr {
				if payload != nil {
					t.Fatalf("endpoint call error respomse = %#v", payload)
				}
				return
			}

			got, ok := payload.(service.AdditionResponse)
			if !ok {
				t.Fatalf("Bad endpoint response type = %T", payload)
			}

			if !reflect.DeepEqual(got, tt.response) {
				t.Errorf("endpoint response = %#v, want %#v", got, tt.response)
			}
		})
	}
}

func Test_makeDivisionEndpoint(t *testing.T) {
	goodServer := newStringServer(`{"Result": 20}`)
	defer goodServer.Close()
	zeroServer := newStringServer(`{"Result": 0, "Error": "divide by zero"}`)
	defer zeroServer.Close()
	badServer := newStringServer("")
	defer badServer.Close()

	tests := []struct {
		name     string
		server   *httptest.Server
		request  interface{}
		response service.DivisionResponse
		wantErr  bool
	}{
		{
			"normal",
			goodServer,
			service.DivisionRequest{
				Dividend: 40,
				Divisor:  2,
			},
			service.DivisionResponse{
				Result: 20,
				Error:  "",
			},
			false,
		},
		{
			"zero",
			zeroServer,
			service.DivisionRequest{
				Dividend: 40,
				Divisor:  0,
			},
			service.DivisionResponse{
				Result: 0,
				Error:  "divide by zero",
			},
			false,
		},
		{
			name:    "badServer",
			server:  badServer,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u, err := url.Parse(tt.server.URL)
			if err != nil {
				t.Fatal("Cannot parse httptest server url:", tt.server.URL)
			}

			endpoint := makeDivisionEndpoint(u)
			payload, err := endpoint(context.Background(), tt.request)
			if (err != nil) != tt.wantErr {
				t.Fatalf("endpoint call error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.wantErr {
				if payload != nil {
					t.Fatalf("endpoint call error respomse = %#v", payload)
				}
				return
			}

			got, ok := payload.(service.DivisionResponse)
			if !ok {
				t.Fatalf("Bad endpoint response type = %T", payload)
			}

			if !reflect.DeepEqual(got, tt.response) {
				t.Errorf("endpoint response = %#v, want %#v", got, tt.response)
			}
		})
	}
}

func Test_makeMultiplicationEndpoint(t *testing.T) {
	goodServer := newStringServer(`{"Result": 80}`)
	defer goodServer.Close()
	badServer := newStringServer("")
	defer badServer.Close()

	tests := []struct {
		name     string
		server   *httptest.Server
		request  interface{}
		response service.MultiplicationResponse
		wantErr  bool
	}{
		{
			"normal",
			goodServer,
			service.MultiplicationRequest{
				A: 40,
				B: 2,
			},
			service.MultiplicationResponse{
				Result: 80,
			},
			false,
		},
		{
			name:    "badServer",
			server:  badServer,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u, err := url.Parse(tt.server.URL)
			if err != nil {
				t.Fatal("Cannot parse httptest server url:", tt.server.URL)
			}

			endpoint := makeMultiplicationEndpoint(u)
			payload, err := endpoint(context.Background(), tt.request)
			if (err != nil) != tt.wantErr {
				t.Fatalf("endpoint call error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.wantErr {
				if payload != nil {
					t.Fatalf("endpoint call error respomse = %#v", payload)
				}
				return
			}

			got, ok := payload.(service.MultiplicationResponse)
			if !ok {
				t.Fatalf("Bad endpoint response type = %T", payload)
			}

			if !reflect.DeepEqual(got, tt.response) {
				t.Errorf("endpoint response = %#v, want %#v", got, tt.response)
			}
		})
	}
}

func Test_makeSubtractionEndpoint(t *testing.T) {
	goodServer := newStringServer(`{"Result": 38}`)
	defer goodServer.Close()
	badServer := newStringServer("")
	defer badServer.Close()

	tests := []struct {
		name     string
		server   *httptest.Server
		request  interface{}
		response service.SubtractionResponse
		wantErr  bool
	}{
		{
			"normal",
			goodServer,
			service.SubtractionRequest{
				Subtrahend: 40,
				Minuend:    2,
			},
			service.SubtractionResponse{
				Result: 38,
			},
			false,
		},
		{
			name:    "badServer",
			server:  badServer,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u, err := url.Parse(tt.server.URL)
			if err != nil {
				t.Fatal("Cannot parse httptest server url:", tt.server.URL)
			}

			endpoint := makeSubtractionEndpoint(u)
			payload, err := endpoint(context.Background(), tt.request)
			if (err != nil) != tt.wantErr {
				t.Fatalf("endpoint call error = %v, wantErr %v", err, tt.wantErr)
			}
			if tt.wantErr {
				if payload != nil {
					t.Fatalf("endpoint call error respomse = %#v", payload)
				}
				return
			}

			got, ok := payload.(service.SubtractionResponse)
			if !ok {
				t.Fatalf("Bad endpoint response type = %T", payload)
			}

			if !reflect.DeepEqual(got, tt.response) {
				t.Errorf("endpoint response = %#v, want %#v", got, tt.response)
			}
		})
	}
}

func newStringServer(response string) *httptest.Server {
	return httptest.NewServer(stdhttp.HandlerFunc(func(w stdhttp.ResponseWriter, r *stdhttp.Request) {
		_, _ = w.Write([]byte(response))
	}))
}
