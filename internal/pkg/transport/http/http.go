// Copyright © 2020 VERDOÏA Laurent
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package http

import (
	"net/http"

	kitendpoint "github.com/go-kit/kit/endpoint"
	kittransport "github.com/go-kit/kit/transport/http"
	gptendpoint "gitlab.com/verdoiadottech/go-project-template/internal/pkg/endpoint"
	mygraphql "gitlab.com/verdoiadottech/go-project-template/pkg/graphql"
)

// HandlerSet collects all of the endpoints that compose an add service.
type HandlerSet struct {
	AdditionHandler       http.Handler
	SubtractionHandler    http.Handler
	MultiplicationHandler http.Handler
	DivisionHandler       http.Handler
	GraphqlHandler        http.Handler
}

// NewHandlerSet create a new set of HTTP handler that compose a CalculatorService.
func NewHandlerSet(e gptendpoint.Set) HandlerSet {
	set := HandlerSet{}

	set.AdditionHandler = makeAdditionHandler(e.AdditionEndpoint)
	set.SubtractionHandler = makeSubtractionHandler(e.SubtractionEndpoint)
	set.MultiplicationHandler = makeMultiplicationHandler(e.MultiplicationEndpoint)
	set.DivisionHandler = makeDivisionHandler(e.DivisionEndpoint)
	set.GraphqlHandler = makeGraphqlHandler(e.GraphqlEndpoint)

	return set
}

func makeAdditionHandler(e kitendpoint.Endpoint) http.Handler {
	return kittransport.NewServer(
		e,
		decodeAdditionRequest,
		kittransport.EncodeJSONResponse,
	)
}

func makeSubtractionHandler(e kitendpoint.Endpoint) http.Handler {
	return kittransport.NewServer(
		e,
		decodeSubtractionRequest,
		kittransport.EncodeJSONResponse,
	)
}

func makeMultiplicationHandler(e kitendpoint.Endpoint) *kittransport.Server {
	return kittransport.NewServer(
		e,
		decodeMultiplicationRequest,
		kittransport.EncodeJSONResponse,
	)
}

func makeDivisionHandler(e kitendpoint.Endpoint) http.Handler {
	return kittransport.NewServer(
		e,
		decodeDivisionRequest,
		kittransport.EncodeJSONResponse,
	)
}

func makeGraphqlHandler(e kitendpoint.Endpoint) http.Handler {
	h := kittransport.NewServer(
		e,
		mygraphql.DecodeGraphqlRequest,
		mygraphql.EncodeGraphqlResponse,
	)

	return mygraphql.HandleCORSProtocol(h)
}
