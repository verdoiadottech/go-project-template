// Copyright © 2020 VERDOÏA Laurent
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package endpoint

import (
	"context"

	"github.com/go-kit/kit/endpoint"
	"github.com/graphql-go/handler"
	"gitlab.com/verdoiadottech/go-project-template/internal/pkg/service"
)

// Set collects all of the endpoints that compose a CalculatorService.
type Set struct {
	AdditionEndpoint       endpoint.Endpoint
	SubtractionEndpoint    endpoint.Endpoint
	MultiplicationEndpoint endpoint.Endpoint
	DivisionEndpoint       endpoint.Endpoint
	GraphqlEndpoint        endpoint.Endpoint
}

// NewSet create a new set of endpoints that compose a CalculatorService.
func NewSet(svc service.Calculator) Set {
	return Set{
		AdditionEndpoint:       makeAdditionEndpoint(svc),
		SubtractionEndpoint:    makeSubtractionEndpoint(svc),
		MultiplicationEndpoint: makeMultiplicationEndpoint(svc),
		DivisionEndpoint:       makeDivisionEndpoint(svc),
		GraphqlEndpoint:        makeGraphqlEndpoint(svc),
	}
}

func makeAdditionEndpoint(svc service.Calculator) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		req := request.(service.AdditionRequest)
		res := svc.Addition(req.A, req.B)
		return service.AdditionResponse{Result: res}, nil
	}
}

func makeSubtractionEndpoint(svc service.Calculator) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		req := request.(service.SubtractionRequest)
		res := svc.Subtraction(req.Subtrahend, req.Minuend)
		return service.SubtractionResponse{Result: res}, nil
	}
}

func makeMultiplicationEndpoint(svc service.Calculator) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		req := request.(service.MultiplicationRequest)
		res := svc.Multiplication(req.A, req.B)
		return service.MultiplicationResponse{Result: res}, nil
	}
}

func makeDivisionEndpoint(svc service.Calculator) endpoint.Endpoint {
	return func(_ context.Context, request interface{}) (interface{}, error) {
		req := request.(service.DivisionRequest)
		result, err := svc.Division(req.Dividend, req.Divisor)
		response := service.DivisionResponse{
			Result: result,
		}

		if err != nil {
			response.Error = err.Error()
		}

		return response, nil
	}
}

func makeGraphqlEndpoint(svc service.Calculator) endpoint.Endpoint {
	return func(ctx context.Context, request interface{}) (interface{}, error) {
		req := request.(*handler.RequestOptions)
		res := svc.Graphql(ctx, req)
		return res, nil
	}
}
