// Copyright © 2020 VERDOÏA Laurent
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package service

import (
	"context"
	"fmt"
	"time"

	"github.com/go-kit/kit/metrics"
	"github.com/graphql-go/graphql"
	"github.com/graphql-go/handler"
)

// MetricSet collects all of the metrics which are observable within the service.
type MetricSet struct {
	RequestCount          metrics.Counter
	RequestLatency        metrics.Histogram
	CountResult           metrics.Histogram
	GraphqlRequestCount   metrics.Counter
	GraphqlRequestLatency metrics.Histogram
}

// InstrumentingMiddleware returns a service middleware that instruments
// the number of integers summed and characters concatenated over the lifetime of
// the service.
func InstrumentingMiddleware(metrics MetricSet) Middleware {
	return func(next Calculator) Calculator {
		return instrumentingMiddleware{
			requestCount:          metrics.RequestCount,
			requestLatency:        metrics.RequestLatency,
			countResult:           metrics.CountResult,
			graphqlRequestCount:   metrics.GraphqlRequestCount,
			graphqlRequestLatency: metrics.GraphqlRequestLatency,
			next:                  next,
		}
	}
}

type instrumentingMiddleware struct {
	requestCount          metrics.Counter
	requestLatency        metrics.Histogram
	countResult           metrics.Histogram
	graphqlRequestCount   metrics.Counter
	graphqlRequestLatency metrics.Histogram
	next                  Calculator
}

// Compile check...
var _ Calculator = (*instrumentingMiddleware)(nil)

func (mw instrumentingMiddleware) Addition(a, b float64) (res float64) {
	defer mw.deferredInstrumentation(time.Now(), "addition", &res, nil)
	return mw.next.Addition(a, b)
}

func (mw instrumentingMiddleware) Subtraction(subtrahend, minuend float64) (res float64) {
	defer mw.deferredInstrumentation(time.Now(), "subtraction", &res, nil)
	return mw.next.Subtraction(subtrahend, minuend)
}

func (mw instrumentingMiddleware) Multiplication(subtrahend, minuend float64) (res float64) {
	defer mw.deferredInstrumentation(time.Now(), "multiplication", &res, nil)
	return mw.next.Multiplication(subtrahend, minuend)
}

func (mw instrumentingMiddleware) Division(dividend, divisor float64) (res float64, err error) {
	defer mw.deferredInstrumentation(time.Now(), "division", &res, &err)
	return mw.next.Division(dividend, divisor)
}

func (mw instrumentingMiddleware) Graphql(ctx context.Context, request *handler.RequestOptions) *graphql.Result {
	defer func(begin time.Time) {
		mw.graphqlRequestLatency.Observe(time.Since(begin).Seconds())
		mw.graphqlRequestCount.Add(1)
	}(time.Now())

	return mw.next.Graphql(ctx, request)
}

func (mw instrumentingMiddleware) deferredInstrumentation(begin time.Time, method string, res *float64, err *error) {
	lvs := []string{"method", method, "error", fmt.Sprint(err != nil && *err != nil)}
	mw.requestCount.With(lvs...).Add(1)
	mw.requestLatency.With(lvs...).Observe(time.Since(begin).Seconds())
	mw.countResult.Observe(*res)
}
