// Copyright © 2020 VERDOÏA Laurent
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package service

import (
	"context"
	"time"

	"github.com/graphql-go/graphql"
	"github.com/graphql-go/handler"
	"github.com/sirupsen/logrus"
)

// LoggingMiddleware takes a logger as a dependency
// and returns a service Middleware.
func LoggingMiddleware(logger *logrus.Logger) Middleware {
	return func(next Calculator) Calculator {
		return loggingMiddleware{logger, next}
	}
}

type loggingMiddleware struct {
	logger *logrus.Logger
	next   Calculator
}

// Compile check...
var _ Calculator = (*loggingMiddleware)(nil)

func (mw loggingMiddleware) Addition(a, b float64) (res float64) {
	defer func(begin time.Time) {
		mw.logger.WithFields(logrus.Fields{
			"method":       "addition",
			"leftOperand":  a,
			"rightOperand": b,
			"result":       res,
			"took":         time.Since(begin),
		}).Info()
	}(time.Now())

	res = mw.next.Addition(a, b)
	return
}

func (mw loggingMiddleware) Subtraction(subtrahend, minuend float64) (res float64) {
	defer func(begin time.Time) {
		mw.logger.WithFields(logrus.Fields{
			"method":     "subtraction",
			"subtrahend": subtrahend,
			"minuend":    minuend,
			"result":     res,
			"took":       time.Since(begin),
		}).Info()
	}(time.Now())

	res = mw.next.Subtraction(subtrahend, minuend)
	return
}

func (mw loggingMiddleware) Multiplication(subtrahend, minuend float64) (res float64) {
	defer func(begin time.Time) {
		mw.logger.WithFields(logrus.Fields{
			"method":     "multiplication",
			"subtrahend": subtrahend,
			"minuend":    minuend,
			"result":     res,
			"took":       time.Since(begin),
		}).Info()
	}(time.Now())

	res = mw.next.Multiplication(subtrahend, minuend)
	return
}

func (mw loggingMiddleware) Division(dividend, divisor float64) (res float64, err error) {
	defer func(begin time.Time) {
		l := mw.logger.WithFields(logrus.Fields{
			"method":   "division",
			"dividend": dividend,
			"divisor":  divisor,
			"result":   res,
			"took":     time.Since(begin),
		})
		if err != nil {
			l = l.WithError(err)
		}
		l.Info()
	}(time.Now())

	res, err = mw.next.Division(dividend, divisor)
	return
}

func (mw loggingMiddleware) Graphql(ctx context.Context, request *handler.RequestOptions) (res *graphql.Result) {
	defer func(begin time.Time) {
		mw.logger.WithFields(logrus.Fields{
			"method": "graphql",
			"took":   time.Since(begin),
		}).Info("GraphQL call")
	}(time.Now())

	return mw.next.Graphql(ctx, request)
}
