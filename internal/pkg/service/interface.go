// Copyright © 2020 VERDOÏA Laurent
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package service

import (
	"context"

	"github.com/graphql-go/graphql"
	"github.com/graphql-go/handler"
)

// Middleware is a chainable behavior modifier for Calculator
type Middleware func(Calculator) Calculator

// Calculator expose a simple calculator
type Calculator interface {
	Addition(a, b float64) float64
	Subtraction(subtrahend, minuend float64) float64
	Multiplication(a, b float64) float64
	Division(dividend, divisor float64) (float64, error)
	Graphql(ctx context.Context, request *handler.RequestOptions) *graphql.Result
}

// AdditionRequest is used to request the Calculator.Addition service
type AdditionRequest struct {
	A float64 `json:"a"`
	B float64 `json:"b"`
}

// AdditionResponse is used for the answer of the Calculator.Addition service
type AdditionResponse struct {
	Result float64 `json:"result"`
}

// SubtractionRequest is used to request the Calculator.Subtraction service
type SubtractionRequest struct {
	Subtrahend float64 `json:"subtrahend"`
	Minuend    float64 `json:"minuend"`
}

// SubtractionResponse is used to request the Calculator.Subtraction service
type SubtractionResponse struct {
	Result float64 `json:"result"`
}

// MultiplicationRequest is used to request the Calculator.Multiplication service
type MultiplicationRequest struct {
	A float64 `json:"a"`
	B float64 `json:"b"`
}

// MultiplicationResponse is used to request the Calculator.Multiplication service
type MultiplicationResponse struct {
	Result float64 `json:"result"`
}

// DivisionRequest is used to request the Calculator.Division service
type DivisionRequest struct {
	Dividend float64 `json:"dividend"`
	Divisor  float64 `json:"divisor"`
}

// DivisionResponse is used to request the Calculator.Division service
type DivisionResponse struct {
	Result float64 `json:"result"`
	Error  string  `json:"error,omitempty"` // errors do not JSON-marshal, so we use a string
}
