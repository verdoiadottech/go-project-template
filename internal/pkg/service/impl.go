// Copyright © 2020 VERDOÏA Laurent
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package service

import (
	"context"
	"errors"

	"github.com/graphql-go/graphql"
	"github.com/graphql-go/handler"
	"github.com/sirupsen/logrus"
)

// NewCalculatorService returns a basic CalculatorService with all of the expected middlewares wired in.
func NewCalculatorService(logger *logrus.Logger, metrics MetricSet) (Calculator, error) {
	svc, err := NewBasicCalculatorService()
	if err != nil {
		return nil, err
	}

	svc = LoggingMiddleware(logger)(svc)
	svc = InstrumentingMiddleware(metrics)(svc)

	return svc, nil
}

// NewBasicCalculatorService returns a naive, stateless implementation of CalculatorService.
func NewBasicCalculatorService() (Calculator, error) {
	c := &calculator{}

	if err := c.buildSchema(); err != nil {
		return nil, err
	}

	return c, nil
}

type calculator struct {
	schema graphql.Schema
}

// Compile check...
var _ Calculator = (*calculator)(nil)

func (c *calculator) Addition(a, b float64) float64 {
	return a + b
}

func (c *calculator) Subtraction(subtrahend, minuend float64) float64 {
	return subtrahend - minuend
}

func (c *calculator) Multiplication(a, b float64) float64 {
	return a * b
}

func (c *calculator) Division(dividend, divisor float64) (float64, error) {
	if divisor == 0. {
		return 0., errors.New("divide by zero")
	}

	return dividend / divisor, nil
}

func (c *calculator) Graphql(ctx context.Context, request *handler.RequestOptions) *graphql.Result {
	params := graphql.Params{
		Context:        ctx,
		OperationName:  request.OperationName,
		RequestString:  request.Query,
		Schema:         c.schema,
		VariableValues: request.Variables,
	}

	return graphql.Do(params)
}

func (c *calculator) buildSchema() error {
	fields := graphql.Fields{
		"addition": &graphql.Field{
			Type: graphql.Float,
			Args: graphql.FieldConfigArgument{
				"a": &graphql.ArgumentConfig{
					Type: graphql.Float,
				},
				"b": &graphql.ArgumentConfig{
					Type: graphql.Float,
				},
			},

			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				a := p.Args["a"].(float64)
				b := p.Args["b"].(float64)
				return c.Addition(a, b), nil
			},
		},
		"subtraction": &graphql.Field{
			Type: graphql.Float,
			Args: graphql.FieldConfigArgument{
				"subtrahend": &graphql.ArgumentConfig{
					Type:        graphql.Float,
					Description: "pouet",
				},
				"minuend": &graphql.ArgumentConfig{
					Type: graphql.Float,
				},
			},

			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				subtrahend := p.Args["subtrahend"].(float64)
				minuend := p.Args["minuend"].(float64)
				return c.Subtraction(subtrahend, minuend), nil
			},
		},
		"multiplication": &graphql.Field{
			Type: graphql.Float,
			Args: graphql.FieldConfigArgument{
				"a": &graphql.ArgumentConfig{
					Type: graphql.Float,
				},
				"b": &graphql.ArgumentConfig{
					Type: graphql.Float,
				},
			},

			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				a := p.Args["a"].(float64)
				b := p.Args["b"].(float64)
				return c.Multiplication(a, b), nil
			},
		},
		"division": &graphql.Field{
			Type: graphql.Float,
			Args: graphql.FieldConfigArgument{
				"dividend": &graphql.ArgumentConfig{
					Type: graphql.Float,
				},
				"divisor": &graphql.ArgumentConfig{
					Type: graphql.Float,
				},
			},

			Resolve: func(p graphql.ResolveParams) (interface{}, error) {
				dividend := p.Args["dividend"].(float64)
				divisor := p.Args["divisor"].(float64)
				res, err := c.Division(dividend, divisor)
				if err != nil {
					return nil, err
				}
				return res, nil
			},
		},
	}

	rootQuery := graphql.ObjectConfig{Name: "Query", Fields: fields}
	schemaConfig := graphql.SchemaConfig{Query: graphql.NewObject(rootQuery)}

	schema, err := graphql.NewSchema(schemaConfig)
	if err != nil {
		return err
	}

	c.schema = schema

	return nil
}
