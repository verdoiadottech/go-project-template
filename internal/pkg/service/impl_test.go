// Copyright © 2020 VERDOÏA Laurent
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package service

import (
	"testing"
)

func Test_calculator_Addition(t *testing.T) {
	type args struct {
		a float64
		b float64
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{"zero", args{0, 0}, 0},
		{"simple", args{1, 1}, 2},
		{"negative", args{1, -1}, 0},
		{"universe", args{40, 2}, 42},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c, err := NewBasicCalculatorService()
			if err != nil {
				t.Fatalf("Cannot instantiate service: %#v", err)
			}

			if got := c.Addition(tt.args.a, tt.args.b); got != tt.want {
				t.Errorf("Addition() = %#v, want %#v", got, tt.want)
			}
		})
	}
}

func Test_calculator_Division(t *testing.T) {
	type args struct {
		dividend float64
		divisor  float64
	}
	tests := []struct {
		name    string
		args    args
		want    float64
		wantErr bool
	}{
		{"zero", args{0, 0}, 0, true},
		{"simple", args{1, 1}, 1, false},
		{"negative", args{1, -1}, -1, false},
		{"universe", args{40, 2}, 20, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c, err := NewBasicCalculatorService()
			if err != nil {
				t.Fatalf("Cannot instantiate service: %#v", err)
			}

			got, err := c.Division(tt.args.dividend, tt.args.divisor)
			if (err != nil) != tt.wantErr {
				t.Errorf("Division() error = %#v, wantErr %#v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Division() got = %#v, want %#v", got, tt.want)
			}
		})
	}
}

func Test_calculator_Multiplication(t *testing.T) {
	type args struct {
		a float64
		b float64
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{"zero", args{0, 0}, 0},
		{"simple", args{1, 1}, 1},
		{"negative", args{1, -1}, -1},
		{"universe", args{40, 2}, 80},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c, err := NewBasicCalculatorService()
			if err != nil {
				t.Fatalf("Cannot instantiate service: %#v", err)
			}

			if got := c.Multiplication(tt.args.a, tt.args.b); got != tt.want {
				t.Errorf("Multiplication() = %#v, want %#v", got, tt.want)
			}
		})
	}
}

func Test_calculator_Subtraction(t *testing.T) {
	type args struct {
		subtrahend float64
		minuend    float64
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{"zero", args{0, 0}, 0},
		{"simple", args{1, 1}, 0},
		{"negative", args{1, -1}, 2},
		{"universe", args{40, 2}, 38},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c, err := NewBasicCalculatorService()
			if err != nil {
				t.Fatalf("Cannot instantiate service: %#v", err)
			}

			if got := c.Subtraction(tt.args.subtrahend, tt.args.minuend); got != tt.want {
				t.Errorf("Subtraction() = %#v, want %#v", got, tt.want)
			}
		})
	}
}
