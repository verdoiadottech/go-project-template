// Copyright © 2020 VERDOÏA Laurent
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package request

import (
	"context"
	"fmt"
	"strconv"

	"github.com/sirupsen/logrus"
	"gitlab.com/verdoiadottech/go-project-template/internal/app"
	"gitlab.com/verdoiadottech/go-project-template/internal/pkg/service"
	"gitlab.com/verdoiadottech/go-project-template/internal/pkg/transport/http"
)

// Run the request command
func Run(c *app.Config, operation, leftOperand, rightOperand string) {
	set := http.NewClientSet(c.Address)
	w, err := buildWrapper(set, operation)
	if err != nil {
		logrus.WithError(err).Fatal("cannot create client")
	}

	var left float64
	left, err = strconv.ParseFloat(leftOperand, 64)
	if err != nil {
		logrus.WithField("input", leftOperand).WithError(err).Fatal("Cannot parse left operand")
	}

	var right float64
	right, err = strconv.ParseFloat(rightOperand, 64)
	if err != nil {
		logrus.WithField("input", leftOperand).WithError(err).Fatal("Cannot parse right operand")
	}

	if res, err := w(context.Background(), left, right); err != nil {
		logrus.WithError(err).Fatal("API call error")
	} else {
		logrus.WithField("res", res).Info("API call done")
	}
}

type wrapper func(ctxt context.Context, leftOperand, rightOperand float64) (interface{}, error)

func buildWrapper(set http.ClientSet, operation string) (wrapper, error) {
	switch operation {
	case "addition":
		return makeAdditionWrapper(set), nil
	case "subtraction":
		return makeSubtractionWrapper(set), nil
	case "multiplication":
		return makeMultiplicationWrapper(set), nil
	case "division":
		return makeDivisionWrapper(set), nil
	default:
		return nil, fmt.Errorf("unknow operation: %s", operation)
	}
}

func makeAdditionWrapper(set http.ClientSet) wrapper {
	return func(ctxt context.Context, leftOperand, rightOperand float64) (response interface{}, err error) {
		req := service.AdditionRequest{
			A: leftOperand,
			B: rightOperand,
		}

		return set.AdditionEndpoint(ctxt, req)
	}
}

func makeSubtractionWrapper(set http.ClientSet) wrapper {
	return func(ctxt context.Context, leftOperand, rightOperand float64) (response interface{}, err error) {
		req := service.SubtractionRequest{
			Subtrahend: leftOperand,
			Minuend:    rightOperand,
		}

		return set.SubtractionEndpoint(ctxt, req)
	}
}

func makeMultiplicationWrapper(set http.ClientSet) wrapper {
	return func(ctxt context.Context, leftOperand, rightOperand float64) (response interface{}, err error) {
		req := service.MultiplicationRequest{
			A: leftOperand,
			B: rightOperand,
		}

		return set.MultiplicationEndpoint(ctxt, req)
	}
}

func makeDivisionWrapper(set http.ClientSet) wrapper {
	return func(ctxt context.Context, leftOperand, rightOperand float64) (response interface{}, err error) {
		req := service.DivisionRequest{
			Dividend: leftOperand,
			Divisor:  rightOperand,
		}

		return set.DivisionEndpoint(ctxt, req)
	}
}
