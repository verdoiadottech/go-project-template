// Copyright © 2020 VERDOÏA Laurent
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package serve

import (
	"context"
	"errors"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/sirupsen/logrus"
	"gitlab.com/verdoiadottech/go-project-template/internal/app"
	gptendpoint "gitlab.com/verdoiadottech/go-project-template/internal/pkg/endpoint"
	gptservice "gitlab.com/verdoiadottech/go-project-template/internal/pkg/service"
	gpttransport "gitlab.com/verdoiadottech/go-project-template/internal/pkg/transport/http"
)

// Run the serve command
func Run(c *app.Config) {
	metrics := buildMetrics()
	svc, err := gptservice.NewCalculatorService(logrus.StandardLogger(), metrics)
	if err != nil {
		logrus.WithError(err).Fatal("Cannot instantiate an new calculator service")
	}

	endpoints := gptendpoint.NewSet(svc)
	handlers := gpttransport.NewHandlerSet(endpoints)

	mux := http.NewServeMux()
	mux.Handle("/api/addition", handlers.AdditionHandler)
	mux.Handle("/api/subtraction", handlers.SubtractionHandler)
	mux.Handle("/api/multiplication", handlers.MultiplicationHandler)
	mux.Handle("/api/division", handlers.DivisionHandler)
	mux.Handle("/metrics", promhttp.Handler())
	mux.Handle("/graphql", handlers.GraphqlHandler)

	s := &http.Server{
		Addr:    c.Address,
		Handler: mux,
	}

	var shutdownErr error
	onShutdown := func() {
		if err := s.Shutdown(context.Background()); err != nil {
			shutdownErr = err
		}
	}

	var runErr error
	run := func() {
		logrus.WithField("address", c.Address).Infof("Will listen and serve")
		if err := s.ListenAndServe(); err != nil && !errors.Is(err, http.ErrServerClosed) {
			runErr = err
		}
		logrus.Info("Bye, gpt...")
	}

	foreground(run, onShutdown)

	if runErr != nil {
		logrus.WithError(runErr).Errorf("Cannot listen and serve")
	}

	if shutdownErr != nil {
		logrus.WithError(runErr).Warn("Cannot gracefully shutdown")
	}
}

func foreground(run, onShutdown func()) {
	done := make(chan struct{})
	signals := make(chan os.Signal, 1)
	signal.Notify(signals, syscall.SIGINT, syscall.SIGTERM)

	go func() {
		if signal, ok := <-signals; ok {
			logrus.WithField("signal", signal).Debug("Signal received")
			onShutdown()
		} else {
			logrus.Trace("Stop signal goroutine")
		}
		close(done)
	}()

	run()
	signal.Stop(signals)

	// Unblock watchdog routine but do not wait until it finished
	close(signals)
}
