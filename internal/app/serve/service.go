// Copyright © 2020 VERDOÏA Laurent
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package serve

import (
	kitprometheus "github.com/go-kit/kit/metrics/prometheus"
	stdprometheus "github.com/prometheus/client_golang/prometheus"
	"gitlab.com/verdoiadottech/go-project-template/internal/pkg/service"
)

func buildMetrics() service.MetricSet {
	var metrics service.MetricSet

	fieldKeys := []string{"method", "error"}
	metrics.RequestCount = kitprometheus.NewCounterFrom(stdprometheus.CounterOpts{
		Namespace: "app",
		Subsystem: "calculator_service",
		Name:      "request_count",
		Help:      "Number of requests received.",
	}, fieldKeys)
	metrics.RequestLatency = kitprometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
		Namespace: "app",
		Subsystem: "calculator_service",
		Name:      "request_latency_microseconds",
		Help:      "Total duration of requests in microseconds.",
	}, fieldKeys)
	metrics.CountResult = kitprometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
		Namespace: "app",
		Subsystem: "calculator_service",
		Name:      "count_result",
		Help:      "The result of each count method.",
	}, []string{})
	metrics.GraphqlRequestCount = kitprometheus.NewCounterFrom(stdprometheus.CounterOpts{
		Namespace: "app",
		Subsystem: "calculator_service",
		Name:      "graphql_request_count",
		Help:      "Number of requests received with GraphQL.",
	}, []string{})
	metrics.GraphqlRequestLatency = kitprometheus.NewSummaryFrom(stdprometheus.SummaryOpts{
		Namespace: "app",
		Subsystem: "calculator_service",
		Name:      "graphql_request_latency_microseconds",
		Help:      "Total duration of GraphQL requests in microseconds.",
	}, []string{})

	return metrics
}
