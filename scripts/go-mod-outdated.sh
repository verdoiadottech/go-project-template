#!/usr/bin/env bash
# Copyright © 2020 VERDOÏA Laurent
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -o errexit
set -o nounset
set -o pipefail

# Be sure to be at the root folder of the repository.
cd "$(realpath "$(dirname "$0")/..")"

# Directory change permit to no pollute our go.mod/go.sum file
cd "${TMPDIR:-/tmp}"
go get -u github.com/psampaz/go-mod-outdated@v0.6.0
cd - > /dev/null

go list -u -m -json all | go-mod-outdated -direct -ci
