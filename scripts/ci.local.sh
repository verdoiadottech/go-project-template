#!/usr/bin/env bash
# Copyright © 2020 VERDOÏA Laurent
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -o errexit
set -o nounset
#set -o xtrace

# Be sure to be at the root folder of the repository.
cd "$(realpath "$(dirname "$0")/..")"

dockerRun() {
  docker run \
    --tty --rm \
    --env PI_DEBUG_TRACE="${PI_DEBUG_TRACE:-}" \
    --env LOCALHOST_CI=true \
    --mount "type=bind,src=${PWD},dst=/opt/src,consistency=delegated" --workdir /opt/src \
    "$@"
}

# Run inside a docker with /.cache/go mounted as $GOPATH.
# When architecture differ between client and server or when we cannot detect local GOPATH installation,
# we use /.cache/go instead.
# Architecture check is useful for mac user because docker run in VM.
dockerRunGoCached() {
  local client
  client=$(docker version --format '{{.Client.Os}}/{{.Client.Arch}}')
  local server
  server=$(docker version --format '{{.Server.Os}}/{{.Server.Arch}}')
  local gopath=
  local gocache=

  if [ "${client}" = "${server}" ] && command -v go &> /dev/null; then
    gopath=$(go env GOPATH)
    if [ -z "${gopath}" ]; then
      # shellcheck disable=SC2016
      echo 'Cannot detect $(go env GOPATH)' >&2
    fi

    gocache=$(go env GOCACHE)
    if [ -z "${gocache}" ]; then
      # shellcheck disable=SC2016
      echo 'Cannot detect $(go env GOCACHE)' >&2
    fi
  fi

  if [ -z "${gopath}" ]; then
    gopath="${PWD}/.cache/go/gopath"
    if [ ! -d "${gopath}" ]; then
      mkdir -p "${gopath}"
    fi
  fi

  if [ -z "${gocache}" ]; then
    gocache="${PWD}/.cache/go/gocache"
    if [ ! -d "${gocache}" ]; then
      mkdir -p "${gocache}"
    fi
  fi

  dockerRun \
    --env GOPATH=/go \
    --env GOCACHE=/root/.cache/go-build \
    --mount "type=bind,src=${gopath},dst=/go,consistency=delegated" \
    --mount "type=bind,src=${gocache},dst=/root/.cache/go-build,consistency=delegated" \
    "$@"
}

dockerRunGolangciLint() {
  local folder=${PWD}/.cache/golangci-lint

  if [ ! -d "${folder}" ]; then
    mkdir -p "${folder}"
  fi

  dockerRunGoCached \
    --entrypoint sh \
    --mount "type=bind,src=${folder},dst=/root/.cache/golangci-lint,consistency=delegated" \
    golangci/golangci-lint:v1.30.0-alpine \
    "$@"
}

runJob() {
  local jobName=$1
  shift

  echo -e "\e[7m\e[33m---> Run job ${jobName}\e[39m\e[27m"
  if "$@"; then
    echo -e "\e[7m\e[32m<--- Job ${jobName} succeed\e[39m\e[27m\n"
  else
    echo -e "\e[7m\e[31m<--- Job \e[5m${jobName}\e[25m failed\e[39m\e[27m"
    return 1
  fi
}

failed=()
tryJob() {
  if runJob "$@"; then
    return
  fi

  failed+=("$1")
}

launchJob() {
  case "$1" in
    cc-coverage) tryJob "$1" dockerRun --entrypoint sh golang:1.15.0-buster "./build/ci/jobs/$1.sh" ;;
    go-build) tryJob "$1" dockerRunGoCached golang:1.15.0-buster "./build/ci/jobs/$1.sh" ;;
    go-mod-download) tryJob "$1" dockerRunGoCached golang:1.15.0-buster "./build/ci/jobs/$1.sh" ;;
    go-test) tryJob "$1" dockerRunGoCached golang:1.15.0-buster "./build/ci/jobs/$1.sh" ;;
    gofmt) tryJob "$1" dockerRunGoCached golang:1.15.0-buster "./build/ci/jobs/$1.sh" ;;
    golangci-lint) tryJob "$1" dockerRunGolangciLint "./build/ci/jobs/$1.sh" ;;
    goreleaser) tryJob "$1" dockerRunGoCached --entrypoint sh goreleaser/goreleaser:v0.141.0 "./build/ci/jobs/$1.sh" ;;
    markdownlint) tryJob "$1" dockerRun --entrypoint sh github/super-linter:V3.8.0 "./build/ci/jobs/$1.sh" ;;
    mkdocs) tryJob "$1" dockerRun --entrypoint sh lvjplabs/mkdocs:1.1.2-r1 "./build/ci/jobs/$1.sh" ;;
    shellcheck) tryJob "$1" dockerRun --entrypoint sh github/super-linter:V3.8.0 "./build/ci/jobs/$1.sh" ;;
    shfmt) tryJob "$1" dockerRun --entrypoint sh mvdan/shfmt:v3.1.2-alpine "./build/ci/jobs/$1.sh" ;;
    sonar-scanner) tryJob "$1" dockerRun --entrypoint sh sonarsource/sonar-scanner-cli:4.4 "./build/ci/jobs/$1.sh" ;;
    yamllint) tryJob "$1" dockerRun --entrypoint sh github/super-linter:V3.8.0 "./build/ci/jobs/$1.sh" ;;
    *)
      echo "Unknown job: $1"
      exit 1
      ;;
  esac
}

# go-mod-download is intended to be run inside a CI pipeline
ALL=(
  "shfmt"
  "shellcheck"
  "gofmt"
  "golangci-lint"
  "yamllint"
  "markdownlint"
  "go-test"
  "cc-coverage"
  "go-build"
  "mkdocs"
  "goreleaser"
)

if [ $# -gt 0 ]; then
  while [ $# -gt 0 ]; do
    launchJob "$1"
    shift
  done
else
  for job in "${ALL[@]}"; do
    launchJob "${job}"
  done
fi

if [ "${#failed[@]}" -eq 0 ]; then
  echo -e "🍺🍺🍺 \e[32mPipeline succeed !!!\e[39m 🍺🍺🍺"
else
  echo -e "💥💥💥 \e[31mPipeline failed ;-(\e[39m 💥💥💥"
  for jobName in "${failed[@]}"; do
    echo -e "    💣 \e[31m${jobName}\e[39m 💣"
  done
fi
