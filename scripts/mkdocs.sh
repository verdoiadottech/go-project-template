#!/usr/bin/env bash
# Copyright © 2020 VERDOÏA Laurent
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

set -o errexit
set -o nounset
set -o pipefail

cd "$(realpath "$(dirname "$0")/../website")"

cp ../dist/go-test/coverage.html docs/ || true

exec docker run \
  --tty --rm \
  --mount "type=bind,source=${PWD},target=/work,ro" \
  --workdir /work \
  --publish 8000:8000 \
  lvjplabs/mkdocs:1.1.2-r0 \
  serve \
  --dev-addr 0.0.0.0:8000 \
  "$@"
