// Copyright © 2020 VERDOÏA Laurent
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package cmd

import (
	"fmt"
	"io"
	"os"
	"path"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/verdoiadottech/go-project-template/internal/app"
)

var cfgFile string
var cfg *app.Config
var logLevel string
var verbose bool

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "gpt",
	Short: "gpt is a little opinionated go example project",
	Long: `gpt purpose is to promote an opinionated go project template.
	It's implement a little server which do expose basic elementary arithmetic through web services.`,
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initLog)
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().StringVarP(&cfgFile, "config", "c", "", "config file (default is $HOME/.config/gpt.yaml)")
	rootCmd.PersistentFlags().StringVarP(&logLevel, "log", "", "info", "log level (default is info)")
	rootCmd.PersistentFlags().BoolVarP(&verbose, "verbose", "v", false, "set log level to DEBUG")
}

// initLog configure logrus
func initLog() {
	if err := setupLogs(os.Stdout, logLevel); err != nil {
		fmt.Printf("Cannot setup logs infrastructure: %s\n", err)
		os.Exit(1)
	}
}

func setupLogs(out io.Writer, level string) error {
	log.SetOutput(out)
	log.SetFormatter(&log.JSONFormatter{})

	lvl, err := log.ParseLevel(level)
	if err != nil {
		return err
	}

	if verbose && log.DebugLevel > lvl {
		lvl = log.DebugLevel
	}

	log.SetLevel(lvl)
	return nil
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	// read in `GPT_` prefixed environment variables that match
	viper.SetEnvPrefix("gpt")
	viper.AutomaticEnv()
	viper.SetConfigType("yaml")

	tryDefault := false

	// Set default config file path
	if cfgFile == "" {
		tryDefault = true
		// Find home directory.
		cfgDir, err := os.UserConfigDir()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}

		cfgFile = path.Join(cfgDir, "gpt", "config.yaml")
		getLogger(cfgFile).Debug("Try to load default config file")
	}

	var file *os.File
	var err error
	file, err = os.Open(cfgFile)
	if err != nil {
		if !tryDefault {
			getLogger(cfgFile).WithError(err).Fatal("Cannot open config file")
		}
	}
	defer file.Close()

	if err := viper.ReadConfig(file); err != nil {
		getLogger(cfgFile).WithError(err).Fatal("Cannot read config file")
	}

	cfg = new(app.Config)
	if err := viper.Unmarshal(cfg); err != nil {
		getLogger(cfgFile).WithError(err).Fatal("Cannot interpret config file")
	}
}

func getLogger(cfgFile string) *log.Entry {
	return log.WithField("path", cfgFile)
}
