package graphql

import (
	"bytes"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

const dummyBodyRequest = "DUMMY_BODY_REQUEST"
const dummyBodyResponse = "DUMMY_BODY_ERSPONSE"

type dummyHandler struct {
	CallCount int
}

func (dh *dummyHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	_, _ = w.Write([]byte(dummyBodyResponse))
	dh.CallCount++
}

func TestHandleCORSProtocol_passTrough(t *testing.T) {
	methods := []string{
		http.MethodGet,
		http.MethodHead,
		http.MethodPost,
		http.MethodPut,
		http.MethodPatch,
		http.MethodDelete,
		http.MethodConnect,
		http.MethodTrace,
	}

	for _, method := range methods {
		t.Run(method, func(t *testing.T) {
			dummy := dummyHandler{}
			cors := HandleCORSProtocol(&dummy)
			requestBody := bytes.NewBuffer([]byte(dummyBodyRequest))
			request := httptest.NewRequest(method, "/foo/bar", requestBody)
			recorder := httptest.NewRecorder()
			cors.ServeHTTP(recorder, request)
			response := recorder.Result()
			if dummy.CallCount != 1 {
				t.Fatalf("handler call count = %d, want = 1", dummy.CallCount)
			}

			responseBody, _ := ioutil.ReadAll(response.Body)
			if !bytes.Equal(responseBody, []byte(dummyBodyResponse)) {
				t.Fatalf(
					"response body = %v, want = %v",
					responseBody,
					[]byte(dummyBodyRequest),
				)
			}
		})
	}
}

func TestHandleCORSProtocol_options(t *testing.T) {
	dummy := dummyHandler{}
	cors := HandleCORSProtocol(&dummy)
	requestBody := bytes.NewBuffer([]byte(dummyBodyRequest))
	request := httptest.NewRequest(http.MethodOptions, "/foo/bar", requestBody)
	recorder := httptest.NewRecorder()
	cors.ServeHTTP(recorder, request)
	response := recorder.Result()
	if dummy.CallCount != 0 {
		t.Errorf("handler call count = %d, want = 0", dummy.CallCount)
	}

	responseBody, _ := ioutil.ReadAll(response.Body)
	if len(responseBody) != 0 {
		t.Errorf("Body was emit = %v", responseBody)
	}

	headers := []string{
		headerOrigin,
		headerMethods,
		headerHeaders,
	}

	for _, header := range headers {
		if _, present := response.Header[header]; !present {
			t.Errorf("Header not found: %s", header)
		}
	}
}
