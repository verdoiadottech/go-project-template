// Copyright © 2020 VERDOÏA Laurent
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package graphql

import (
	"net/http"
)

const (
	headerOrigin  = "Access-Control-Allow-Origin"
	headerMethods = "Access-Control-Allow-Methods"
	headerHeaders = "Access-Control-Allow-Headers"
)

// HandleCORSProtocol will handle CORS-preflight requests which by definition
// the OPTIONS HTTP method.
// Be aware that 'Access-Control-Allow-Origin' will be set to '*'.
func HandleCORSProtocol(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set(headerOrigin, "*")
		w.Header().Set(headerMethods, "GET, POST, OPTIONS")
		w.Header().Set(headerHeaders, "Origin, Content-Type")

		if r.Method == "OPTIONS" {
			return
		}

		handler.ServeHTTP(w, r)
	})
}
