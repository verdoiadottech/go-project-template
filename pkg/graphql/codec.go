// Copyright © 2020 VERDOÏA Laurent
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package graphql

import (
	"context"
	"encoding/json"
	"net/http"

	"github.com/graphql-go/handler"
)

// DecodeGraphqlRequest deserialize a GraphQL request as specified in
// https://graphql.org/learn/serving-over-http/ and return a
// *graphql.RequestOptions.
func DecodeGraphqlRequest(_ context.Context, r *http.Request) (interface{}, error) {
	return handler.NewRequestOptions(r), nil
}

// EncodeGraphqlResponse serialize a GraphQL response qs specified in
// https://graphql.org/learn/serving-over-http/.
// Since internal errors are embedded in the JSON errors fields,
// if response is an error 500 status code will be used, otherwise 200.
func EncodeGraphqlResponse(_ context.Context, w http.ResponseWriter, response interface{}) error {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")

	// We suppose that an error was encountered before the graphql execution begins.
	if err, ok := response.(error); ok {
		w.WriteHeader(http.StatusInternalServerError)
		payload := map[string]interface{}{
			"errors": []map[string]interface{}{
				{
					"message": err.Error(),
				},
			},
		}

		return json.NewEncoder(w).Encode(payload)
	}

	w.WriteHeader(http.StatusOK)
	return json.NewEncoder(w).Encode(response)
}
