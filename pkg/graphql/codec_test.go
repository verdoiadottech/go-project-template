// Copyright © 2020 VERDOÏA Laurent
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package graphql

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"

	"github.com/graphql-go/graphql"
	"github.com/graphql-go/handler"
)

func TestDecodeGraphqlRequest(t *testing.T) {
	// Only test nominal case since we do not test underlying library

	want := &handler.RequestOptions{
		Query:         "query MyGraphqlQuery { hello }",
		Variables:     nil,
		OperationName: "MyNilQuery",
	}

	payload, err := json.Marshal(want)
	if err != nil {
		t.Fatalf("Cannot marshal request: %v", err)
	}

	reqBody := bytes.NewBuffer(payload)
	req := httptest.NewRequest("POST", "/foo/bar", reqBody)

	raw, err := DecodeGraphqlRequest(context.Background(), req)
	if err != nil {
		t.Fatalf("Error occurred: %v", err)
	}

	var res *handler.RequestOptions
	var goodType bool
	res, goodType = raw.(*handler.RequestOptions)
	if !goodType {
		t.Fatalf("Get response type: %T, want = %T", raw, res)
	}

	if !reflect.DeepEqual(res, want) {
		t.Fatalf("decoded query = %#v, want = %#v", res, want)
	}
}

func TestEncodeGraphqlResponse(t *testing.T) {
	tests := []struct {
		name       string
		input      interface{}
		output     string
		statusCode int
	}{
		{
			name:       "error",
			input:      errors.New("dummy error"),
			output:     `{"errors":[{"message":"dummy error"}]}` + "\n",
			statusCode: http.StatusInternalServerError,
		},
		{
			name: "normal",
			input: &graphql.Result{
				Data: map[string]interface{}{
					"first": 1,
					"last":  999,
				},
			},
			output:     `{"data":{"first":1,"last":999}}` + "\n",
			statusCode: http.StatusOK,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			recorder := httptest.NewRecorder()
			if err := EncodeGraphqlResponse(context.Background(), recorder, tt.input); err != nil {
				t.Fatalf("EncodeGraphqlResponse() error = %v", err)
			}

			response := recorder.Result()

			if response.StatusCode != tt.statusCode {
				t.Errorf("status code = %v, want = %v", response.StatusCode, tt.statusCode)
			}

			body, _ := ioutil.ReadAll(response.Body)
			if !bytes.Equal(body, []byte(tt.output)) {
				t.Errorf("body = %v, want = %v", string(body), tt.output)
			}
		})
	}
}
